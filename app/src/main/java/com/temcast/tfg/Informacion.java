package com.temcast.tfg;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.temcast.tfg.TipoDatos.Datos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Informacion extends AppCompatActivity {
    ArrayList<BarEntry> entries;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion);
        entries=new ArrayList<>();
        RequestQueue requestQueue=Principal.requestQueue;
        String id_habitacion=String.valueOf(getIntent().getExtras().getInt("Estancia"));
        StringRequest re=new StringRequest(Datos.servidor(this)+"/Get_temperatura/"+id_habitacion, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray lis=new JSONArray(response);
                    for(int x=0;x<lis.length();x++){
                        JSONObject js=lis.getJSONObject(x);
                        entries.add(new BarEntry(x,js.getInt("valor")));
                    }
                    BarDataSet dataSet=new BarDataSet(entries,"Temperatura");
                    BarData b=new BarData(dataSet);
                    BarChart chart=(BarChart)findViewById(R.id.chart);
                    chart.setData(b);
                    chart.notifyDataSetChanged();
                    chart.invalidate();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        StringRequest te=new StringRequest(Datos.servidor(this)+"/actual", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    ((TextView)findViewById(R.id.TActual)).setText("La temperatura actual es de : "+jsonObject.getString("valor"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(te);
        requestQueue.add(re);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
