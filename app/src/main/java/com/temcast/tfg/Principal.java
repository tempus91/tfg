package com.temcast.tfg;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.temcast.tfg.TipoDatos.Datos;
import com.temcast.tfg.TipoDatos.Habitaciones;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Principal extends AppCompatActivity {
    public static RequestQueue requestQueue;
    public static  Context c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        requestQueue= Volley.newRequestQueue(this);
        StringRequest request=new StringRequest(Datos.servidor(this)+"/Get_habitaciones", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    ArrayList<Habitaciones> datos= Datos.Get_habitaciones();
                    datos.clear();
                    for(int x=0;x<=jsonArray.length();x++){
                        JSONObject j = jsonArray.getJSONObject(x);
                        Habitaciones l=new Habitaciones(j.getInt("Id"),j.getInt("Tipo"),j.getString("Nombre"));
                        datos.add(l);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(request);
    c=getApplicationContext();
    }
    public void click(View v){
        switch (v.getId()){
            case R.id.acctiones:
                startActivity(new Intent(this,Estancias.class).putExtra("Acction",1));
                break;
            case R.id.informacion:
                startActivity(new Intent(this,Estancias.class).putExtra("Acction",0));
                break;
            case R.id.ajustes:
                startActivity(new Intent(this,Preferencias.class));
        }
    }
    public static void error_voley(){
        new AlertDialog.Builder(c)
                .setTitle("Error con la conexion del servidor revise la conexion se cerrara para evitar mayor problemas")
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                System.exit(0);
            }
        }).setNegativeButton("No cerrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }).show();
    }
}
