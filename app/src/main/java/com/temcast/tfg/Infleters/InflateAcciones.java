package com.temcast.tfg.Infleters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.temcast.tfg.Principal;
import com.temcast.tfg.R;
import com.temcast.tfg.TipoDatos.Datos;
import com.temcast.tfg.TipoDatos.Opciones;
import java.util.List;
/**
 * Created by Jorge on 20/04/2017.
 */

public class InflateAcciones extends BaseAdapter {
    List<Opciones> l;
    Context c;
    RequestQueue requestQueue=Principal.requestQueue;
    public InflateAcciones(List<Opciones> l, Context c){
        this.l=l;
        this.c=c;
    }
    @Override
    public int getCount() {
        return l.size();
    }

    @Override
    public Object getItem(int position) {
        return l.get(position);
    }

    @Override
    public long getItemId(int position) {
        return l.get(position).hashCode();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(l.get(position).Actuador==1){
            convertView= LayoutInflater.from(c).inflate(R.layout.activity_acciones_button,null);
            TextView textView=(TextView)convertView.findViewById(R.id.Accion_button_text);
            final Switch aSwitch=(Switch)convertView.findViewById(R.id.Accrion_button_swich);
            final String metodo=l.get(position).metodo;
            textView.setText(l.get(position).descripcion);
            aSwitch.setChecked(l.get(position).valor==1);
            aSwitch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //ToDo: hacer que se mande la señal al arduino correspondiente
                    String ip=l.get(position).ip;
                    int valor=(aSwitch.isChecked()==true)?1:0;
                    StringRequest l=new StringRequest(Datos.servidor(c)+"/Set_Action/"+ip+"/"+metodo+"/"+String.valueOf(valor), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                    requestQueue.add(l);
                }
            });
        }else if(l.get(position).Actuador==0){
            convertView= LayoutInflater.from(c).inflate(R.layout.activity_acciones_tipos,null);
            SeekBar  seekBar=(SeekBar)convertView.findViewById(R.id.Accion_slider);
            final String metodo=l.get(position).metodo;
            seekBar.setProgress(l.get(position).valor);
            seekBar.setMax(255);
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    String ip=l.get(position).ip;
                    StringRequest l=new StringRequest(Datos.servidor(c)+"/Set_Action/"+ip+"/"+metodo+"/"+seekBar.getProgress(), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                    requestQueue.add(l);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
            ((TextView)convertView.findViewById(R.id.textView2)).setText(l.get(position).descripcion);


        }else{
            System.out.println("Tipo de dato no reconocido");
        }

        return convertView;
    }
}
