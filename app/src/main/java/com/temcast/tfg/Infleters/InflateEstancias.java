package com.temcast.tfg.Infleters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.temcast.tfg.Acciones;
import com.temcast.tfg.TipoDatos.Habitaciones;
import com.temcast.tfg.Informacion;
import com.temcast.tfg.Principal;
import com.temcast.tfg.R;

import java.util.ArrayList;

/**
 * Created by Jorge on 06/04/2017.
 */

public class InflateEstancias extends BaseAdapter {
    ArrayList<Habitaciones> hab;
    Context context;
    int acction;
    public InflateEstancias(ArrayList<Habitaciones> est, Context context, int acction){
        this.hab=est;
        this.context=context;
        this.acction=acction;
    }
    @Override
    public int getCount() {
        return hab.size();
    }

    @Override
    public Object getItem(int position) {
        return hab.get(position);
    }

    @Override
    public long getItemId(int position) {
        return hab.get(position).hashCode();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.celdas_habi,null);
        }
        Button boton=(Button)convertView.findViewById(R.id.button6);
        boton.setId(position);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(acction==1){
                    context.startActivity(new Intent(context,Acciones.class).putExtra("Estancia",hab.get(position).id));
                }else if(acction==0){

                    context.startActivity(new Intent(context,Informacion.class).putExtra("Estancia",hab.get(position).id));
                }
            }
        });
        boton.setText(hab.get(position).nombre);
        Button boton2=(Button)convertView.findViewById(R.id.button5);
        if(position+1<hab.size()){
        boton2.setId(position+1);
        boton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(acction==1){
                    context.startActivity(new Intent(context,Acciones.class).putExtra("Estancia",hab.get(position+1).id));
                }else if(acction==0){
                    context.startActivity(new Intent(context,Informacion.class).putExtra("Estancia",hab.get(position+1).id));
                }
            }
        });
        boton2.setText(hab.get(position+1).nombre);}else {
            boton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
               context.startActivity(new Intent(context,Principal.class));
                }
            });
            boton2.setText("Atras");
        }
        return convertView;
    }
}
