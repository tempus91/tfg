package com.temcast.tfg;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.temcast.tfg.TipoDatos.Blue;
import com.temcast.tfg.TipoDatos.Datos;
import com.temcast.tfg.TipoDatos.Habitaciones;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;
import java.util.UUID;
//Lo primero que realizara sera la vinculacion por bluetooth.

//tendra que llamar al servicio web add_arudino y tratar el string para añadir 1

//A continuación lo que realizara sera mandarle mediante bluetooth la los datos de acceso al wifi

//una vez finalizdo se quedra en bucle un T aprox de 2 minutos para acceder mediante wifi al arduino

// Una vez conectado con el arduino accedera al metodo who i saltara al metodo registrar
public class Preferencias extends AppCompatActivity {



    ArrayList<BluetoothDevice> mArrayAdapter=new ArrayList<>();
    RequestQueue requestQueue=Principal.requestQueue;
    EditText t;
    BluetoothAdapter ba;
    PrintWriter pw;
    String msj;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferencias);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    public void new_ip(View v){
        final Context context=Preferencias.this;
        t=new EditText(this);
        new AlertDialog.Builder(this).setTitle("Añadir nueva ip").setMessage("Indicar la ip o el domino del servidor por ejemplo 192.168.1.1").setView(t).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(Preferencias.this);
                SharedPreferences.Editor prefEditor = sharedPref.edit();
                prefEditor.putString( "IP", "http://"+t.getText().toString()+":8081" );
                prefEditor.apply();
                Toast.makeText(getBaseContext(),"Se ha añadido correctamente la ip",Toast.LENGTH_LONG).show();
                Toast.makeText(getBaseContext(),t.getText().toString(),Toast.LENGTH_LONG).show();
            }
        }).show();
    }
    public void nuevo(View v){
        startActivity(new Intent(this,Formu.class));
    }

    public void nueva_estancia(View v){
        t=new EditText(Preferencias.this);
        new AlertDialog.Builder(this).setTitle("Introduce el nombre de la estancia que deseas añadir").setView(t).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StringRequest request=new StringRequest(Datos.servidor(Preferencias.this) + "/Set_habitacion/" + t.getText().toString(), new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(Preferencias.this,"Se ha añadido correctamente la estancia "+t.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                requestQueue.add(request);
            }
        }).setCancelable(false).show();
    }






}
