package com.temcast.tfg;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.temcast.tfg.Infleters.InflateAcciones;
import com.temcast.tfg.TipoDatos.Datos;
import com.temcast.tfg.TipoDatos.Opciones;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Acciones extends AppCompatActivity {
RequestQueue l;
    Context c=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acciones);
        l=Principal.requestQueue;
        String s=String.valueOf(getIntent().getExtras().getInt("Estancia"));
        StringRequest req=new StringRequest(Datos.servidor(this)+"/Get_acciones/"+s, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray r=new JSONArray(response);
                    InflateAcciones in=new InflateAcciones(ordenar_Lista(r),c);
                    ((ListView) findViewById(R.id.Acciones_Lista)).setAdapter(in);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Principal.error_voley();
            }
        });
        l.add(req);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    private List<Opciones> ordenar_Lista(JSONArray r){
        List<Opciones> lista=new ArrayList<>();
        try {

            for(int x=0;x<r.length();x++){
                JSONObject j=r.getJSONObject(x);
                lista.add(new Opciones(j.getInt("Valor"),j.getInt("Id"),j.getInt("Tipo"),j.getString("Descripcion"),j.getInt("Actuador"),j.getString("Ip"),j.getString("Metodo")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Collections.shuffle(lista);
        return lista;
    }
}
