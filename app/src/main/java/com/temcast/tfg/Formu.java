package com.temcast.tfg;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.temcast.tfg.TipoDatos.Datos;
import com.temcast.tfg.TipoDatos.Habitaciones;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Formu extends AppCompatActivity {
    String msj,ip,estancia;
    EditText t;
    int Id_arduino,estancia_id;
    RequestQueue requestQueue=Principal.requestQueue;
    BluetoothAdapter ba;
    BluetoothSocket s=null;
    String addr;
    ArrayList<Habitaciones> habitaciones=new ArrayList<>();

    @Override
    protected void onPause() {
        super.onPause();
        try {
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formu);
        ba = BluetoothAdapter.getDefaultAdapter();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        requestQueue.add(new StringRequest(Datos.servidor(Formu.this)+"/Get_lhabitaciones", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray j=new JSONArray(response);
                    for(int x=0;x<j.length();x++){
                        JSONObject jsonObject=j.getJSONObject(x);
                        habitaciones.add(new Habitaciones(Integer.parseInt(jsonObject.getString("Id")),0,jsonObject.getString("Nombre")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(ba == null) {
            new AlertDialog.Builder(this)
                    .setTitle("Se requiere bluetooth")
                    .setMessage("Es necesaria la característica de bluetooth para poder enviar comunicaciones al dispositivo")
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
        else if(!ba.isEnabled()) {
            new AlertDialog.Builder(this)
                    .setTitle("Se requiere bluetooth")
                    .setMessage("Es necesario el bluetooth activo para poder enviar comunicaciones al dispositivo")
                    .setCancelable(false)
                    .setPositiveButton("Ir a ajustes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
                        }
                    })
                    .show();
        }
        else if(addr == null) select();
    }

    public void select() {

        final List<BluetoothDevice> d = new ArrayList<>(ba.getBondedDevices());
        new AlertDialog.Builder(this)
                .setTitle("Escoge el dispositivo a conectar")
                .setItems(getDevicesString(d), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BluetoothDevice b = d.get(which);
                        addr = b.getAddress();
                        ba.getRemoteDevice(addr);
                        connect(b);
                    }
                })
                .setCancelable(false)
                .show();
    }
    public void añadir(View v){
        ba.cancelDiscovery();
        StringRequest l=new StringRequest(Datos.servidor(this)+"/Get_ip", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ip=sumip(response);
                EditText pass=(EditText) findViewById(R.id.pass);
                EditText ssid=(EditText) findViewById(R.id.ssid);
                int ts,tp;
                ts=(String.valueOf(ssid.getText()).length()<10)?1:2;
                tp=(String.valueOf(pass.getText()).length()<10)?1:2;
                msj+=String.valueOf(ts)+String.valueOf(ssid.getText()).length()+String.valueOf(ssid.getText())+String.valueOf(tp)+pass.getText().length()+pass.getText();;
                while (msj.length()<=60)msj+=" ";
                msj+="Configurado";
                try {
                    s.getOutputStream().write(msj.getBytes());

                } catch (IOException e) {
                    e.printStackTrace();
                }
                Segunda_fase();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Principal.error_voley();
            }
        });
        requestQueue.add(l);
    }

    private void connect(final BluetoothDevice b) {
        View v = new ProgressBar(this);
        final Dialog d = new AlertDialog.Builder(this)
                .setTitle("Conectando con "+b.getName()+"...")
                .setView(v)
                .setCancelable(false)
                .show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    s = b.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                    s.connect();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            d.dismiss();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    public String[] getDevicesString(List<BluetoothDevice> l) {
        String[] d = new String[l.size()];
        for (BluetoothDevice b: l) {
            d[l.indexOf(b)] = b.getName();
        }
        return d;
    }
    public void conectanto(Dialog d){
        final Dialog di=d;
        new Thread(new Runnable() {
            @Override
            public void run() {
                    StringRequest lk=new StringRequest(Datos.servidor(Formu.this) + "/Set_Action/"+ ip +"/base/null", new Response.Listener<String>() {
                        String datos;
                        @Override
                        public void onResponse(String response) {

                            if(response==null)return;
                            else {
                                datos=response.replace("\'","\"");
                            }
                            try {
                                JSONArray ja=new JSONArray(datos);
                                di.dismiss();
                                registrar(ja);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            conectanto(di);
                        }
                    });
                    requestQueue.add(lk);

            }
        }).start();
    }
    public void Segunda_fase(){
        View v =new ProgressBar(this);
        final Dialog d=new AlertDialog.Builder(this)
                .setTitle("Intentando conectar a sensor")
                .setView(v)
                .setCancelable(false)
                .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Formu.this,Preferencias.class));
                    }
                })
                .show();
        conectanto(d);
    }
    private String sumip(String jip){
        String niip ="";
        try {
            niip=new JSONArray(jip).getJSONObject(0).getString("Ip");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String[] tmp=niip.replace(".","/").split("/");
        int nip=Integer.parseInt(tmp[3])+1;
        if(nip<254) {
            msj=tmp[0].length()+tmp[0]+tmp[1].length()+tmp[1]+tmp[2].length()+tmp[2]+String.valueOf(nip).length()+String.valueOf(nip);
            return ip= tmp[0] + "." + tmp[1] + "." + tmp[2] + "." + String.valueOf(nip);
        }else{
            new AlertDialog.Builder(this).setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(getApplicationContext(),Preferencias.class));
                }
            }).setMessage("Has alcanzado el maximo de dispositivos arduinos que se pueden conectar").create();
            return "null";
        }
    }
    private void registrar(JSONArray ja){

        for(int x=0;x<ja.length();x++) {
            JSONObject j = null;
            try {
                j = ja.getJSONObject(x);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            t = new EditText(this);
            try {
                final String nombre = j.getString("Nombre");
                final String tipo = j.getString("Tipo");
                final String actuador = j.getString(" Actuador");
                final String metodo = j.getString("Metodo");

                if(tipo.equals("1")){
                    new AlertDialog.Builder(this).setTitle("Deseas activar el Actuador "+nombre).setPositiveButton("Si activar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new AlertDialog.Builder(Formu.this).setTitle("Introduce la descripcion de este Actuador").setView(t).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    mandardatos(t.getText().toString(),"1",actuador,requestQueue,estancia,Id_arduino,metodo);
                                }
                            }).setCancelable(false).show();
                        }
                    }).setNegativeButton("No Activar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            return;
                        }
                    }).setCancelable(false).show();
                }else {
                    new AlertDialog.Builder(this).setTitle("Deseas activar el sensor "+nombre).setPositiveButton("Si activar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new AlertDialog.Builder(Formu.this).setTitle("Introduce la descripcion de este sensor").setView(t).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Toast.makeText(Formu.this, t.getText(), Toast.LENGTH_LONG);
                                    mandardatos(nombre,"0",actuador,requestQueue,estancia,Id_arduino,metodo);
                                }
                            }).setCancelable(false).show();
                        }
                    }).setNegativeButton("No Activar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            return;
                        }
                    }).setCancelable(false).show();
                }
                final String  estancias[]=new String[habitaciones.size()];
                for(int y=0;y<habitaciones.size();y++)
                    estancias[y]=habitaciones.get(y).nombre;
                new AlertDialog.Builder(Formu.this).setTitle("Selecciona la habitacion").setItems(estancias, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        estancia=String.valueOf(habitaciones.get(which).id);
                        registrar_arduino(ip,estancia_id);
                    }
                }).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    private void mandardatos(String nombre,String Tipo,String Actuador,RequestQueue requestQueue,String estancia,int Id_arduino,String Metodo){

        StringRequest ta=new StringRequest(Datos.servidor(this)+"/Set_registrar/"+nombre+"/"+Tipo+"/"+Actuador+"/"+estancia+"/"+Id_arduino+"/"+Metodo, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(ta);
    }
    public void registrar_arduino(String ip,int estancia){

        StringRequest ta=new StringRequest(Datos.servidor(this)+"/Set_Arduino/"+ip+estancia, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Id_arduino=Integer.getInteger(new JSONObject(response).getString("Id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(ta);
    }
}
