package com.temcast.tfg.TipoDatos;

import android.support.annotation.NonNull;

/**
 * Created by Jorge on 20/04/2017.
 */

public class Opciones implements Comparable{

    public int id,tipo,valor,Actuador;
    public String descripcion,ip,metodo;
    public Opciones(int valor,int id , int tipo , String descripcion,int actuador,String ip,String metodo){
        this.id=id;
        this.tipo=tipo;
        this.descripcion=descripcion;
        this.valor=valor;
        this.Actuador=actuador;
        this.ip=ip;
        this.metodo=metodo;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return ((Opciones)o).tipo;
    }
}
