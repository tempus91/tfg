package com.temcast.tfg.TipoDatos;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.temcast.tfg.TipoDatos.Habitaciones;

import java.util.ArrayList;

/**
 * Created by Jorge on 11/04/2017.
 */

public class Datos {
    private static ArrayList<Habitaciones> habitacion;

    public static ArrayList<Habitaciones> Get_habitaciones(){
        if(habitacion==null){
            habitacion=new ArrayList<>();
        }
        return habitacion;
    }

    public static String servidor(Context c){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(c);
        String ip="http://192.168.1.2:8081";
        return sharedPref.getString("IP",ip);
    }

}
