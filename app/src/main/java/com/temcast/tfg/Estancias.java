package com.temcast.tfg;

import android.content.Context;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.temcast.tfg.Infleters.InflateEstancias;
import com.temcast.tfg.TipoDatos.Datos;
import com.temcast.tfg.TipoDatos.Habitaciones;

import java.util.ArrayList;

public class Estancias extends AppCompatActivity {
    ArrayList<Habitaciones> habitaciones = Datos.Get_habitaciones();
    int acction;
    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estancias);
        c = getApplicationContext();

        Bundle extre = getIntent().getExtras();
        acction = extre.getInt("Acction");
        ArrayList<Habitaciones> l = new ArrayList<>();
        for (Habitaciones h : habitaciones) {
            boolean esta=false;
            for(Habitaciones li : l){
                if(li.nombre.equals(h.nombre))
                esta=!esta;
            }
            if (h.accion == acction  && !esta) l.add(h);
            else if(l.size()==0){l.add(h);}
        }
        InflateEstancias ie = new InflateEstancias(l, this, acction);
        for (int x = 0; x < l.size(); x += 2) {
            ((LinearLayout) findViewById(R.id.listaEstancias)).addView(ie.getView(x, null, null));
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
